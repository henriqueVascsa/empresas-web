angular.module('app', [])
  .controller('Ctrl', function($scope, $rootScope, $http) {


  	$scope.Info = true;
  	$scope.IsVisible = false;
  	$scope.isActive = false;

    $scope.ShowHide = function () {
        $scope.Info = $scope.Info ? false : true;
        $scope.IsVisible = $scope.IsVisible ? false : true;
        $scope.isActive = !$scope.isActive;
    }

    //console.log($rootScope.uid);

    $http.get('json/enterprises.json').
    then(function(data, status, headers, config) {

      $scope.myJsonData = data;
      $scope.companies = $scope.myJsonData.data.enterprises;

      console.log($scope.companies[1]);

    });


  })
