(function() {
  var app = angular.module('myApp', ['ui.router']);

  app.run(function($rootScope, $location, $state, LoginService, myService) {
    $rootScope.$on('$stateChangeStart',
      function(event, toState, toParams, fromState, fromParams){
          console.log('Changed state to: ' + toState);
      });

      if(!LoginService.isAuthenticated()) {
        $state.transitionTo('login');
      }
  });

  app.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');

    $stateProvider
      .state('login', {
        url : '/login',
        templateUrl : 'login.html',
        controller : 'LoginController'
      })
      /*.state('home', {
        url : '/home',
        templateUrl : 'home.html',
        controller : 'HomeController'
      });*/
  }]);

  app.controller('LoginController', function($scope, $rootScope, $stateParams, $state, LoginService, myService, $window, $http) {
    $rootScope.title = "AngularJS Login Sample";

    $scope.formSubmit = function() {

        var authurl = 'http://54.94.179.135:8090/api/v1/users/auth/sign_in';
        var data = {
                    "email" : $scope.username,
                    //"testeapple@ioasys.com.br",
                    "password" : $scope.password
                    //"12341234"
                  };

        $http.post(authurl, data, { headers: { 'Content-Type': 'application/json' } })
                .success(function(result, status, headers, config) {
                    console.log(result);
                    $rootScope.uid = (headers()['uid']);
                    $rootScope.token = (headers()['access-token']);
                    $rootScope.client = (headers()['client']);
                    //console.log($rootScope.uid);
                    //console.log($rootScope.token);
                    //console.log($rootScope.client);
                    $window.location.href = 'conteudo/conteudo.html';
        });
    };

  });

  app.controller('HomeController', function($scope, $rootScope, $stateParams, $state, LoginService) {
    $rootScope.title = "AngularJS Login Sample";

  });

  app.factory('LoginService', function() {
    var admin = 'admin';
    var pass = 'pass';
    var isAuthenticated = false;

    return {
      login : function(username, password) {
        isAuthenticated = username === admin && password === pass;
        return isAuthenticated;
      },
      isAuthenticated : function() {
        return isAuthenticated;
      }
    };

  });

  app.factory('myService', function() {
 var savedData = {}
 function set(data) {
   savedData = data;
 }
 function get() {
  return savedData;
 }

 return {
  set: set,
  get: get
 }

});

})();
